from CRABClient.UserUtilities import config
config = config()


config.General.workArea = 'project_crab'
config.General.transferOutputs = True
config.General.transferLogs = True


config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'pset.py'
config.JobType.maxMemoryMB = 4999

config.Data.inputDataset = ("/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/"
"Phase2HLTTDRSummer20ReRECOMiniAOD-PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/FEVT")
config.Data.publication = False
config.Data.splitting = 'EventAwareLumiBased'
config.Data.unitsPerJob = 750

config.Site.storageSite = 'T3_US_FNALLPC'