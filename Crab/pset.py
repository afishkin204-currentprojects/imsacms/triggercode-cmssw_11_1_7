
import FWCore.ParameterSet.Config as cms
from Configuration.StandardSequences.Eras import eras
process = cms.Process("MYHLT", eras.Phase2C9)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('Configuration.Geometry.GeometryExtended2026D49Reco_cff')
process.load('Configuration.Geometry.GeometryExtended2026D49_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.RawToDigi_cff')
process.load('Configuration.StandardSequences.Reconstruction_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

# process.maxEvents = cms.untracked.PSet(
#     input  = cms.untracked.int32(1200),
#    output = cms.optional.untracked.allowed(cms.int32,cms.PSet)
# )

process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(''),
    secondaryFileNames = cms.untracked.vstring()
)

from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase2_realistic_T15', '')

# -- L1 emulation -- #
process.load('Configuration.StandardSequences.L1TrackTrigger_cff')
process.load('Configuration.StandardSequences.SimL1Emulator_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.L1TrackTrigger_step = cms.Path(process.L1TrackTrigger)
process.L1simulation_step = cms.Path(process.SimL1Emulator)
process.L1TkMuons.L1TrackInputTag = cms.InputTag("TTTracksFromTrackletEmulation", "Level1TTTracks", "RECO")
# -- #

# -- HLTriggerFinalPath -- #
process.hltTriggerSummaryAOD = cms.EDProducer( "TriggerSummaryProducerAOD",
    moduleLabelPatternsToSkip = cms.vstring(  ),
    processName = cms.string( "@" ),
    moduleLabelPatternsToMatch = cms.vstring( 'hlt*', 'L1Tk*' ),
    throw = cms.bool( False )
)
process.hltTriggerSummaryRAW = cms.EDProducer( "TriggerSummaryProducerRAW",
    processName = cms.string( "@" )
)
process.hltBoolFalse = cms.EDFilter( "HLTBool",
    result = cms.bool( False )
)
process.HLTriggerFinalPath = cms.Path(
    process.hltTriggerSummaryAOD+
    process.hltTriggerSummaryRAW+
    process.hltBoolFalse
)
# -- #
process.load('TriggerCode.DarkPhotonAnalyzer.DarkPhotonAnalyzer_cff')

process.DarkPhotonAnalyzer.debug = cms.untracked.bool(True)

process.testp = cms.EndPath(process.DarkPhotonAnalyzer)

# -- HLT paths -- #
from HLTrigger.PhaseII.Muon.Customizers.loadPhase2MuonHLTPaths_cfi import loadPhase2MuonHLTPaths
process = loadPhase2MuonHLTPaths(process)

process.schedule = cms.Schedule(
    process.L1simulation_step,
    process.L1_SingleTkMuon_22,
    process.L1_DoubleTkMuon_17_8,
    process.L1_TripleTkMuon_5_3_3,
    process.HLT_Mu50_FromL1TkMuon_Open,
    process.HLT_Mu50_FromL1TkMuon,
    process.HLT_IsoMu24_FromL1TkMuon,
    process.HLT_Mu37_Mu27_FromL1TkMuon,
    process.HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_FromL1TkMuon,
    process.HLT_TriMu_10_5_5_DZ_FromL1TkMuon,
    process.HLTriggerFinalPath,
    process.testp
)
# -- #

# -- Test Setup -- #
process.load( "DQMServices.Core.DQMStore_cfi" )
process.DQMStore.enableMultiThread = True

process.GlobalTag.globaltag = "111X_mcRun4_realistic_T15_v3"

# process.source.fileNames = cms.untracked.vstring(
#     "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/A33886D9-A977-FD4B-B06F-3CE1D4D3B0FB.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/DFCD19DF-FC14-A547-9F80-8FC51515A992.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/608AF1DA-D775-BA47-B602-F77865F98DCD.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/26AB054F-1C1F-6C40-B131-CA81F1F308A4.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/29219424-B2C8-4141-9ACC-8D20D6D09234.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/99CCC618-C36F-934E-8BD2-479CA54FDEF8.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/B7F78697-C7B9-4648-A5A6-E2D25B26038B.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/4033AA73-AC8C-114E-B6AB-58730B43FD83.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/2136F4A4-726B-0C45-B3AC-49DBB20AAF82.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/282A0CB9-1A6E-0549-8AF4-F4CB29A792C3.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/A48C66B3-904B-AB45-8A46-FBBA9C064561.root",
# "root://cmsxrootd.fnal.gov//store/mc/Phase2HLTTDRSummer20ReRECOMiniAOD/DarkPhotonToMuMu_M_1p0_LargeDarkWidth_TuneCP5_PSWeights_14TeV_pythia8/FEVT/PU200_withNewMB_111X_mcRun4_realistic_T15_v1_ext1-v1/00000/B01C1A25-EEF8-2543-BB90-FE5070EE8295.root",
    
# )

#TFileService initialization
process.TFileService = cms.Service("TFileService", fileName = cms.string("plots.root"))

process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool( True ),
    numberOfThreads = cms.untracked.uint32( 1 ),
    numberOfStreams = cms.untracked.uint32( 1 ),
    sizeOfStackForThreadsInKB = cms.untracked.uint32( 10*1024 )
)

if 'MessageLogger' in process.__dict__:
    process.MessageLogger.categories.append('TriggerSummaryProducerAOD')
    process.MessageLogger.categories.append('L1GtTrigReport')
    process.MessageLogger.categories.append('L1TGlobalSummary')
    process.MessageLogger.categories.append('HLTrigReport')
    process.MessageLogger.categories.append('FastReport')
    process.MessageLogger.cerr.FwkReport.reportEvery = 1  # 1000
# -- #


from SLHCUpgradeSimulations.Configuration.aging import customise_aging_1000
process = customise_aging_1000(process)

from L1Trigger.Configuration.customisePhase2TTNoMC import customisePhase2TTNoMC
process = customisePhase2TTNoMC(process)

from HLTrigger.Configuration.Eras import modifyHLTforEras
modifyHLTforEras(process)


# process.Timing = cms.Service("Timing",
#     summaryOnly = cms.untracked.bool(True),
#     useJobReport = cms.untracked.bool(True)
# )

# process.SimpleMemoryCheck = cms.Service("SimpleMemoryCheck",
#     ignoreTotal = cms.untracked.int32(1)
# )

# print process.dumpPython()

