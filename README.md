# TriggerAnalysis
This repository contains code to conduct trigger analysis at the trigger simulation level. There are four parts to this repository, which are all subfolders:

 - DPIO: A library which handles the storage of events, storing muons that were in the event and the triggers that activated on the events.
 - DarkPhotonAnalyzer: A EDM plugin which can run on events generated using PYTHIA and stores them in a TTree with a branch for the event number and a branch for the event itself.
 - Crab: A series of python scripts which allow DarkPhotonAnalyzer to run on Crab, or locally (see below).
 - EventProcessing: A root script and a root "library" (separated from the other file for easier legibility; included in the root script) which processes a TTree written by DarkPhotonAnalyzer to make plots (see IMSA-CMS paper for more details).
 
 ## Setup
 TLDR: Set up the code that [builds the triggers for DarkPhotonAnalyzer](https://github.com/khaosmos93/CMSPhase2MuonHLT/tree/master),  then clone this repository in the $CMSSW_BASE/src directory and build it.

As a shell script:
```shell
cmsrel CMSSW_11_1_8
cd CMSSW_11_1_8/src
cmsenv
    
git cms-init
git cms-addpkg HLTrigger/HLTfilters
    
git clone https://github.com/khaosmos93/CMSPhase2MuonHLT.git HLTrigger/PhaseII/python/Muon
git clone https://github.com/IMSA-CMS/TriggerAnalysis.git

scram b -j
```
## Run
### Crab
To run DarkPhotonAnalyzer to process events on Crab, rebuild if you had made any changes, then ``gridsetup`` and run
```shell
crab submit crabConfig.py
```
from the Crab folder.

If you want to run DarkPhotonAnalyzer locally, rebuild if you had made any changes, uncomment lines 17-20 and 90-104 in Crab/pset.py and then run
```shell
cmsRun pset.py
```
By default, DarkPhotonAnalyzer will only store prompt final events. If you want to change this behavior, you can write a new class that conforms to the MuonSelector interface and use the DPIO::Muon's makeFromMuonSelector function.
### EventProcessing
The results from previous runs of EventProcessing/plot_tdr.cc are in the images, images_varsize, trigger_statistics folders. To run plot_tdr.cc again using the test data in results.root, run
```shell
root plot_tdr.cc
```
If you want to run plot_tdr.cc on different files, add/change the files in the TChain of that file. 

By default, plot_tdr.cc will give you highest, second-highest, and third-highest muon pT efficiency plots for each trigger, ranging from muon pTs of 5-100 GeV, that will be saved in .png and .pdf extension formats, as well as total efficiency information about each event, but there are a lot of configuration options that can be found in plot_tdr.cc if you need to change the parameters for the plots.