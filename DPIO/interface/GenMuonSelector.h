// -*- C++ -*-
//
// Package:    DPIO
// Class:      MuonSelector
//
/*
 Description: MuonSelector which selects muons from a GenParticleCollection.

 Implementation:
     [Notes on implementation]
*/
//
//          Original Author:  Ari Fishkin
//         Created:  Thu, 1 Apr 2021 15:52:13 CDT
//
//   
#ifndef DPIO_GENMUONSELECTOR_H
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"
#include "DataFormats/Common/interface/Handle.h"
#include "TriggerCode/DPIO/interface/MuonSelector.h"

namespace DPIO
{
    class GenMuonSelector : public MuonSelector
    {
        public:
            //Builds a GenMuonSelector from a Handle to a GenParticleCollection
            //Can be trivially copied
            GenMuonSelector(edm::Handle<reco::GenParticleCollection> _collectionHandle) : collection(_collectionHandle), position(0) {}
            //Returns the next muon in the collection, when empty returns a null pointer.
            const reco::GenParticle* nextMuon() override;
        private:
            edm::Handle<reco::GenParticleCollection> collection;
            size_t position;
    };
}
#define DPIO_GENMUONSELECTOR_H
#endif