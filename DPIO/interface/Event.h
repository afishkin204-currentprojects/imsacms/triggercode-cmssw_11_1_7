// -*- C++ -*-
//
// Package:    DPIO
// Class:      Event
//
/*
 Description: Stores an event from a generator run.

 Implementation:
     [Notes on implementation]
*/
//
//          Original Author:  Ari Fishkin
//         Created:  Thu, 1 Apr 2021 15:52:13 CDT
//
// 
#ifndef DPIO_EVENT_H
#include <vector>
#include <set>
#include <memory>
#include "TObject.h"
#include "TriggerCode/DPIO/interface/Muon.h"
namespace DPIO
{
    class Event : public TObject
    {
        private:
            std::vector<Muon> muons; // Vector of muons in the event
            std::set<std::string> acceptedTriggers; // Set (for easy searching) of trigger names that accepted this event
        public:
            Event() {}
            Event(const std::vector<Muon>& _muons, const std::set<std::string>& _acceptedTriggers) : muons(_muons), acceptedTriggers(_acceptedTriggers) {}
            std::vector<Muon>& getMuons() {return muons;}
            const std::vector<Muon>& getMuons() const {return muons;}
            std::set<std::string>& getAcceptedTriggers() {return acceptedTriggers;}
            const std::set<std::string>& getAcceptedTriggers() const {return acceptedTriggers;}
ClassDef(Event, 1)
    };
}
#define DPIO_EVENT_H
#endif