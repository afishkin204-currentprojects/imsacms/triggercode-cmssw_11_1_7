// -*- C++ -*-
//
// Package:    DPIO
// Class:      MuonSelector
//
/*
 Description: Interface which selects muons from a collection.

 Implementation:
     [Notes on implementation]
*/
//
//          Original Author:  Ari Fishkin
//         Created:  Thu, 1 Apr 2021 15:52:13 CDT
//
//  
#ifndef DPIO_MUONSELECTOR_H
#include "DataFormats/HepMCCandidate/interface/GenParticle.h"

namespace DPIO
{
    class MuonSelector
    {
        public:
            virtual ~MuonSelector() = default;
            //Returns the next muon in the collection, when empty returns a null pointer.
            virtual const reco::GenParticle* nextMuon() = 0;
    };
}
#define DPIO_MUONSELECTOR_H
#endif
