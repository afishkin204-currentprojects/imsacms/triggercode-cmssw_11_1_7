#include "TriggerCode/DPIO/interface/Muon.h" 

ClassImp(DPIO::Muon)

namespace DPIO
{
    Muon::Muon() 
    {

    }
    Muon::Muon(const reco::GenParticle& particle) :
        pt(particle.pt()),
        eta(particle.eta()),
        phi(particle.phi()),
        mass(particle.mass()),
        vx(particle.vx()),
        vy(particle.vy()),
        vz(particle.vz()),
        collisionId(particle.collisionId()),
        statusCode(particle.status()),
        statusFlags(particle.statusFlags().flags_)
    {
        for(const reco::Candidate* mothers = &particle; mothers; mothers = mothers->mother(0))
        {
            motherPdgIds.push_back(mothers->pdgId());
        }
           
    }
    std::vector<Muon> Muon::makeFromMuonSelector(MuonSelector&& muonSelector)
    {
        std::vector<Muon> muons;
        for(const reco::GenParticle* muon = muonSelector.nextMuon(); muon; muon = muonSelector.nextMuon())
        {
            muons.push_back(Muon(*muon));
        }
        return muons;
    }
}
